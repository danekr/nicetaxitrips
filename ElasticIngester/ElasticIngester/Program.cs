﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using ElasticIngester.Entities;
using Newtonsoft.Json;

namespace ElasticIngester
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            var filename = @"C:\Users\evilc\Desktop\Nice-hackathon\trip_fare_001.csv";
            var filename2 = @"C:\Users\evilc\Desktop\Nice-hackathon\trip_data_1.csv";
            var file = System.IO.File.OpenRead(filename2);


            byte[] readBuffer = new byte[1000];
            int readBytes = 0;

            byte[] messageBuffer = new byte[2000];
            int byteToWrite = 0;

            var strings = new List<string>();
            bool parsedHeader = false;
            bool breakLoop = false;
            while (!breakLoop && (readBytes = file.Read(readBuffer, 0, 1000)) != 0)
            {
                for (int i = 0; i < readBytes; i++)
                {
                    messageBuffer[byteToWrite] = readBuffer[i];
                    if (byteToWrite > 0 && messageBuffer[byteToWrite - 1] == 13 && messageBuffer[byteToWrite] == 10)
                    {
                        var str = Encoding.ASCII.GetString(messageBuffer, 0, byteToWrite);
                        if (!parsedHeader)
                        {
                            parsedHeader = true;
                        }
                        else
                        {
                            strings.Add(str);
                        }

                        // Console.WriteLine("Found row: " + str);


                        byteToWrite = 0;

                        if (strings.Count == 10000)
                        {

                            var message = new StringBuilder();

                            foreach (var data in strings)
                            {
                                try
                                {
                                    var parts = data.Split(',');
                                    var tripData = new TripData()
                                    {
                                        Medallion = parts[0],
                                        Hack_Licence = parts[1],
                                        vendor_id = parts[2],
                                        rate_code = parts[3],
                                        store_and_fwd_flag = parts[4],
                                        pickup_datetime = DateTime.Parse(parts[5]),
                                        dropoff_datetime = DateTime.Parse(parts[6]),
                                        passenger_count = int.Parse(parts[7]),
                                        trip_time_in_secs = int.Parse(parts[8]),
                                        trip_distance = double.Parse(parts[9].Replace(".", ",")),
                                        pickup_location = new Location
                                        {
                                            lon = double.Parse(parts[10].Replace(".", ",")),
                                            lat = double.Parse(parts[11].Replace(".", ","))
                                        },
                                        dropoff_location = new Location
                                        {
                                            lon = double.Parse(parts[12].Replace(".", ",")),
                                            lat = double.Parse(parts[13].Replace(".", ","))
                                        }
                                    };

                                    message.Append(@"{ ""index"": {}} " + Environment.NewLine);
                                    message.Append(JsonConvert.SerializeObject(tripData) + Environment.NewLine);
                                }
                                catch (Exception e)
                                {
                                    Console.WriteLine(e + " " + data);
                                }
                            }

                            using (var httpClient = new HttpClient())
                            {
                                var content = new StringContent(message.ToString(), Encoding.UTF8, "application/json");

                                var result = httpClient.PostAsync("http://localhost:9200/trip_data_bulk/_doc/_bulk", content);
                                result.Wait();

                                var mess = result.Result.Content.ReadAsStringAsync().Result;

                                int k = 32;

                                //var content = new StringContent(JsonConvert.SerializeObject(tripData), Encoding.UTF8, "application/json");

                                //var result = httpClient.PostAsync("http://localhost:9200/trip_data/_doc", content);
                                //result.Wait();

                                //var mess = result.Result.Content.ReadAsStringAsync().Result;

                                //int k = 32;
                            }
                            ////breakLoop = true;

                            strings.Clear();
                        }
                    }
                    else
                    {
                        byteToWrite++;
                    }
                }
            }


            var bulkMessage = @"{ ""index"": {}} 
                                { ""field1"": ""value4""} 
                                { ""index"": {}} 
                                { ""field1"": ""value3""}

                            " + Environment.NewLine;


            using (var httpClient = new HttpClient())
            {
                var content = new StringContent(bulkMessage, Encoding.UTF8, "application/json");

                var result = httpClient.PostAsync("http://localhost:9200/trip_data_test/_doc/_bulk", content);
                result.Wait();

                var mess = result.Result.Content.ReadAsStringAsync().Result;

                int k = 32;
            }

            //foreach (var row in strings)
            //{
            //    //var parts = row.Split(',');


            //    //var trip = new TripFare()
            //    //{
            //    //    Medallion = parts[0],
            //    //    Hack_Licence = parts[1],
            //    //    vendor_id = parts[2],
            //    //    pickup_datetime = parts[3],
            //    //    payment_type = parts[4]
            //    //};

            //    var parts = row.Split(',');
            //    var tripData = new TripData()
            //    {
            //        Medallion = parts[0],
            //        Hack_Licence = parts[1],
            //        vendor_id = parts[2],
            //        rate_code = parts[3],
            //        store_and_fwd_flag = parts[4],
            //        pickup_datetime = DateTime.Parse(parts[5]),
            //        dropoff_datetime = DateTime.Parse(parts[6]),
            //        passenger_count = int.Parse(parts[7]),
            //        trip_time_in_secs = int.Parse(parts[8]),
            //        trip_distance = double.Parse(parts[9].Replace(".",",")),
            //        pickup_location = new Location
            //        {
            //            lon = double.Parse(parts[10].Replace(".",",")),
            //            lat = double.Parse(parts[11].Replace(".",","))
            //        },
            //        dropoff_location = new Location
            //        {
            //            lon = double.Parse(parts[12].Replace(".",",")),
            //            lat = double.Parse(parts[13].Replace(".",","))
            //        }
            //    };

            //    using (var httpClient = new HttpClient())
            //    {
            //        var content = new StringContent(JsonConvert.SerializeObject(tripData),Encoding.UTF8,"application/json");

            //        var result = httpClient.PostAsync("http://localhost:9200/trip_data/_doc", content);
            //        result.Wait();

            //        var mess = result.Result.Content.ReadAsStringAsync().Result;

            //        int k = 32;
            //    }
            //}



            //using (var httpClient = new HttpClient())
            //{
            //   var content = new StringContent(serializedObject,Encoding.UTF8,"application/json");

            //   var result = httpClient.PutAsync("http://localhost:9200/twitter/_doc/1", content);
            //    result.Wait();

            //   var mess = result.Result.Content.ReadAsStringAsync().Result;
            //}


            Console.ReadKey();
            //var file = System.IO.File.
        }
    }

    internal class Location
    {
        public double lon { get; set; }
        public double lat { get; set; }
    }
}
