﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ElasticIngester.Entities
{
    class TripData
    {
        public string Medallion { get; set; }
        public string Hack_Licence { get; set; }
        public string vendor_id { get; set; }
        public string rate_code { get; set; }
        public string store_and_fwd_flag { get; set; }
        public DateTime pickup_datetime { get; set; }
        public DateTime dropoff_datetime { get; set; }
        public int passenger_count { get; set; }
        public int trip_time_in_secs { get; set; }
        public double trip_distance { get;set; }
        public Location pickup_location { get; set; }
        public Location dropoff_location { get; set; }
    }
}
