﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ElasticIngester
{
    class TripFare
    {
        public string Medallion { get; set; }
        public string Hack_Licence { get; set; }
        public string vendor_id { get; set; }
        public string pickup_datetime { get; set; }
        public string payment_type { get; set; }
        public string fare_amount { get; set; }
        public string surcharge { get; set; }
        public string mta_tax { get;set; }
        public string tip_amount { get;set; }
        public string tolls_amount { get;set; }
        public string total_amount { get; set; }
    }
}
